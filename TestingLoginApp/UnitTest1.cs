﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace TestingLoginApp
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {

            //Driver for chrome - For running automated tests
            IWebDriver driver = new ChromeDriver();

            //Providing URL to be tested
            driver.Url = "http://localhost:63152";

            //Maximize opened window
            driver.Manage().Window.Maximize();

            //Setting response time
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            //find following elements by id and fill the record and press enter
            driver.FindElement(By.Id("username")).SendKeys("mazdaq");
            driver.FindElement(By.Id("password")).SendKeys("123456" + Keys.Enter);
        }
    }
}
